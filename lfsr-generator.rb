#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>


#Usage: lsfr-generator.rb <coefficients e.g. 0100100010> <IV e.g. 000100100>

coefficients = ARGV.shift.chars.map{|x| Integer(x)}
cells = ARGV.shift.chars.map{|x| Integer(x)} #the IV

loop do
	next_x = 0
	cells.zip(coefficients).each do |cell, coefficient|
		next_x ^= cell & coefficient
	end
	print cells.pop
	cells.unshift(next_x)
end
