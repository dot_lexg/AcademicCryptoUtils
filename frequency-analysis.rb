#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>


frequencies = Hash.new(0)
ARGF.each_char do |c|
	frequencies[c] += 1
end

frequencies.sort_by{|k,v| [-v, k]}.each do |c, count|
	puts "#{c.inspect}: #{count}"
end
