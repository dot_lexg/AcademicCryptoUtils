#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>

ALPHABET = ("A".."Z").to_a

#Affine encryption: y ≡ ax + b mod 26
#Affine decryption: x ≡ a^-1*(y - b) mod 26
def list_translation_table(a, b)
	puts "for y = #{a}x + #{b}:"
	(0...ALPHABET.length).each do |x|
		 y = (a * x + b) % ALPHABET.length
		 puts "#{ALPHABET[x]}(#{x}) => #{ALPHABET[y]}(#{y})"
	end
	puts
end

puts "run `irb -r ./#{__FILE__}` instead" if $0 == __FILE__
