#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>


direction = :encrypt
if ARGV[0] == "-d"
	direction = :decrypt
	ARGV.shift
end

message = ARGV.shift
key = ARGV.shift

if !message || !key
	$stderr.puts "Usage: #{$0} [-d] <message> <key>\n\nWhere 'message' and 'key' are directly on the commandline."
	exit 1
end

raise "message is #{message.length} characters long, but key is #{key.long} characters long" if message.length != key.length

message.chars.zip(key.upcase.chars).each do |m, k|
	k_num = if ("A".."Z").include? k
		k.ord - "A".ord
	else
		0
	end

	k_num = -k_num if direction == :decrypt

	if ("a".."z").include? m
		print ((m.ord - "a".ord + k_num) % 26 + "a".ord).chr
	elsif ("A".."A").include? m
		print ((m.ord - "A".ord + k_num) % 26 + "A".ord).chr
	else
		print m
	end
end

puts
