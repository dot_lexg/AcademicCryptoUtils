#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>


IP_FUNC = [6, 14, 22, 30, 38, 46, 54, 62, 4, 12, 20, 28, 36, 44, 52, 60, 2, 10, 18, 26, 34, 42, 50, 58, 0, 8, 16, 24, 32, 40, 48, 56, 7, 15, 23, 31, 39, 47, 55, 63, 5, 13, 21, 29, 37, 45, 53, 61, 3, 11, 19, 27, 35, 43, 51, 59, 1, 9, 17, 25, 33, 41, 49, 57]
IPINV_FUNC = [24, 56, 16, 48, 8, 40, 0, 32, 25, 57, 17, 49, 9, 41, 1, 33, 26, 58, 18, 50, 10, 42, 2, 34, 27, 59, 19, 51, 11, 43, 3, 35, 28, 60, 20, 52, 12, 44, 4, 36, 29, 61, 21, 53, 13, 45, 5, 37, 30, 62, 22, 54, 14, 46, 6, 38, 31, 63, 23, 55, 15, 47, 7, 39]
E_FUNC = [0, 31, 30, 29, 28, 27, 28, 27, 26, 25, 24, 23, 24, 23, 22, 21, 20, 19, 20, 19, 18, 17, 16, 15, 16, 15, 14, 13, 12, 11, 12, 11, 10, 9, 8, 7, 8, 7, 6, 5, 4, 3, 4, 3, 2, 1, 0, 31]
P_FUNC = [16, 25, 12, 11, 3, 20, 4, 15, 31, 17, 9, 6, 27, 14, 1, 22, 30, 24, 8, 18, 0, 5, 29, 23, 13, 19, 2, 26, 10, 21, 28, 7]

S_BOXES = [[
	13,  1,  2, 15,  8, 13,  4,  8,  6, 10, 15,  3, 11,  7,  1,  4, 10, 12,  9,  5,  3,  6, 14, 11,  5,  0,  0, 14, 12,  9,  7,  2,
	 7,  2, 11,  1,  4, 14,  1,  7,  9,  4, 12, 10, 14,  8,  2, 13,  0, 15,  6, 12, 10,  9, 13,  0, 15,  3,  3,  5,  5,  6,  8, 11],[
	 4, 13, 11,  0,  2, 11, 14,  7, 15,  4,  0,  9,  8,  1, 13, 10,  3, 14, 12,  3,  9,  5,  7, 12,  5,  2, 10, 15,  6,  8,  1,  6,
	 1,  6,  4, 11, 11, 13, 13,  8, 12,  1,  3,  4,  7, 10, 14,  7, 10,  9, 15,  5,  6,  0,  8, 15,  0, 14,  5,  2,  9,  3,  2, 12],[
	12, 10,  1, 15, 10,  4, 15,  2,  9,  7,  2, 12,  6,  9,  8,  5,  0,  6, 13,  1,  3, 13,  4, 14, 14,  0,  7, 11,  5,  3, 11,  8,
	 9,  4, 14,  3, 15,  2,  5, 12,  2,  9,  8,  5, 12, 15,  3, 10,  7, 11,  0, 14,  4,  1, 10,  7,  1,  6, 13,  0, 11,  8,  6, 13],[
	 2, 14, 12, 11,  4,  2,  1, 12,  7,  4, 10,  7, 11, 13,  6,  1,  8,  5,  5,  0,  3, 15, 15, 10, 13,  3,  0,  9, 14,  8,  9,  6,
	 4, 11,  2,  8,  1, 12, 11,  7, 10,  1, 13, 14,  7,  2,  8, 13, 15,  6,  9, 15, 12,  0,  5,  9,  6, 10,  3,  4,  0,  5, 14,  3],[
	 7, 13, 13,  8, 14, 11,  3,  5,  0,  6,  6, 15,  9,  0, 10,  3,  1,  4,  2,  7,  8,  2,  5, 12, 11,  1, 12, 10,  4, 14, 15,  9,
	10,  3,  6, 15,  9,  0,  0,  6, 12, 10, 11,  1,  7, 13, 13,  8, 15,  9,  1,  4,  3,  5, 14, 11,  5, 12,  2,  7,  8,  2,  4, 14],[
	10, 13,  0,  7,  9,  0, 14,  9,  6,  3,  3,  4, 15,  6,  5, 10,  1,  2, 13,  8, 12,  5,  7, 14, 11, 12,  4, 11,  2, 15,  8,  1,
	13,  1,  6, 10,  4, 13,  9,  0,  8,  6, 15,  9,  3,  8,  0,  7, 11,  4,  1, 15,  2, 14, 12,  3,  5, 11, 10,  5, 14,  2,  7, 12],[
	15,  3,  1, 13,  8,  4, 14,  7,  6, 15, 11,  2,  3,  8,  4, 14,  9, 12,  7,  0,  2,  1, 13, 10, 12,  6,  0,  9,  5, 11, 10,  5,
	 0, 13, 14,  8,  7, 10, 11,  1, 10,  3,  4, 15, 13,  4,  1,  2,  5, 11,  8,  6, 12,  7,  6, 12,  9,  0,  3,  5,  2, 14, 15,  9],[
	14,  0,  4, 15, 13,  7,  1,  4,  2, 14, 15,  2, 11, 13,  8,  1,  3, 10, 10,  6,  6, 12, 12, 11,  5,  9,  9,  5,  0,  3,  7,  8,
	 4, 15,  1, 12, 14,  8,  8,  2, 13,  4,  6,  9,  2,  1, 11,  7, 15,  5, 12, 11,  9,  3,  7, 14,  3, 10, 10,  0,  5,  6,  0, 13]]

PC1_FUNC = [7, 15, 23, 31, 39, 47, 55, 63, 6, 14, 22, 30, 38, 46, 54, 62, 5, 13, 21, 29, 37, 45, 53, 61, 4, 12, 20, 28, 1, 9, 17, 25, 33, 41, 49, 57, 2, 10, 18, 26, 34, 42, 50, 58, 3, 11, 19, 27, 35, 43, 51, 59, 36, 44, 52, 60]
PC2_FUNC = [42, 39, 45, 32, 55, 51, 53, 28, 41, 50, 35, 46, 33, 37, 44, 52, 30, 48, 40, 49, 29, 36, 43, 54, 15, 4, 25, 19, 9, 1, 26, 16, 5, 11, 23, 8, 12, 7, 17, 0, 22, 3, 10, 14, 6, 20, 27, 24]

def permute(input, block)
	block.map do |x|
		input[x]
	end.reduce {|o, x| o << 1 | x }
end

def s_box(input)
	(0...8).map do |box|
		S_BOXES[box][input >> (box * 6) & 0b111111]
	end.reverse.reduce {|o, x| o << 4 | x }
end

def swap_halves(x)
	x >> 32 | (x & 0xFFFFFFFF) << 32
end

def key_rot(key, amount)
	upper = key >> 28 & 0xFFFFFFF
	lower = key & 0xFFFFFFF
	upper = upper >> (28 - amount) | (upper << amount & 0xFFFFFFF)
	lower = lower >> (28 - amount) | (lower << amount & 0xFFFFFFF)
	upper << 28 | lower
end

def fmt_bits(x, width, group: 8)
	"\e[33m#{("%0#{width}b" % x).gsub(/(.{#{group}})/, '\1 ')} (0x#{"%0#{width/4}x" % x})\e[0m"
end

def fmt_halves(x)
	"\e[33m#{("%064b" % x).gsub(/(.{8})/, '\1 ').sub(/(.{36})/, '\1| ')} (0x#{("%016x" % x).sub(/(.{8})/, '\1 0x')})\e[0m"
end

if ARGV.length < 2 || ARGV.length > 3 || ARGV.include?("--help")
	$stderr.puts "Usage: ruby #{$0} <plaintextblock> <key>"
	$stderr.puts "Express both as 64 bit hex values (that is, 16 hex digits long)."
	exit 1
end

plaintext = ARGV.shift
key = ARGV.shift
verify_ciphertext = ARGV.shift #Hidden third paramater to test if the calculated ciphertext matches an expected value
raise "plaintext must be 64 bits, run with --help for help" if plaintext.length != 16
raise "key must be 64 bits, run with --help for help" if key.length != 16
raise "ciphertext must be 64 bits, run with --help for help" if verify_ciphertext && verify_ciphertext.length != 16
plaintext = [plaintext].pack("H*").unpack1("Q>")
key = [key].pack("H*").unpack1("Q>")
verify_ciphertext = [verify_ciphertext].pack("H*").unpack1("Q>") if verify_ciphertext

puts "plaintext:  #{fmt_bits(plaintext, 64)}"
puts "key:        #{fmt_bits(key, 64)}"
key = permute(key, PC1_FUNC)
puts "key (PC-1): #{fmt_bits(key, 56)}"
puts
intermediate = permute(plaintext, IP_FUNC)
(1..16).each do |round|
	puts "Round #{round}: #{"-" * 120}"
	puts "halves at beginning of round: #{fmt_halves(intermediate)}"
	rotate_amount = [1, 2, 9, 16].include?(round) ? 1 : 2
	key = key_rot(key, rotate_amount)
	subkey = permute(key, PC2_FUNC)
	puts "key rotl #{rotate_amount}: #{fmt_bits(key, 56)}"
	puts
	f_expanded = permute(intermediate, E_FUNC)
	puts "inside F(): (expanded R_#{round}) #{fmt_bits(f_expanded, 48)}"
	puts "          : xor (subkey_#{round}) #{fmt_bits(subkey, 48)}"
	f_expanded ^= subkey
	puts "          :#{" "*round.digits.length}            => #{fmt_bits(f_expanded, 48)}"
	puts "          : Input to S-Boxes : #{fmt_bits(f_expanded, 48, group: 6)}"
	f_sboxed = s_box(f_expanded)
	puts "          : Output           : #{fmt_bits(f_sboxed, 32, group: 4)}"
	f_result = permute(f_sboxed, P_FUNC)
	puts "          : Output (permuted): #{fmt_bits(f_result, 32)}"
	puts

	intermediate = swap_halves(intermediate)
	puts "                        #{fmt_halves(intermediate)}"
	puts "                    xor #{" " * 38}#{fmt_bits(f_result, 32)}"
	intermediate ^= f_result

	puts "halves at end of round: #{fmt_halves(intermediate)}"
	puts
end

intermediate = swap_halves(intermediate)
puts "halves after final swap: #{fmt_halves(intermediate)}"
ciphertext = permute(intermediate, IPINV_FUNC)
puts "ciphertext: #{fmt_bits(ciphertext, 64)}"

if verify_ciphertext
	if ciphertext == verify_ciphertext
		puts "Verified!"
	else
		puts "Doesn't match! should be:"
		puts "            #{fmt_bits(verify_ciphertext, 64)}"
		exit 1
	end
end
