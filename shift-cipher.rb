#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>


decrypt = false
if ARGV[0] == "-d"
	ARGV.shift
	decrypt = true
end

transfer = {}
shift = (26 - (ARGV.shift.ord - ARGV.shift.ord)) % 26
("a".."z").each do |before|
	after = ((before.ord - "a".ord + shift) % 26 + "a".ord).chr
	if decrypt
		transfer[after] = before
	else
		transfer[before] = after
	end
end


def ansi_red(str)
	"\e[31m#{str}\e[0m"
end

puts "Shift by: #{shift}"
$stdin.each_char do |c|
	if transfer.has_key?(c)
		print transfer[c]
	else
		print ansi_red(c)
	end
end
