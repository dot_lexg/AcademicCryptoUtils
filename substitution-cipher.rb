#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>

decrypt = false
if ARGV[0] == "-d"
	ARGV.shift
	decrypt = true
end

transfer = {}

File.open(ARGV[0], "r") do |file|
	while line = file.gets
		before, after = line.chomp.split(" ")
		next unless before && after
		if decrypt
			transfer[after] = before
		else
			transfer[before] = after
		end
	end
end

def ansi_red(str)
	"\e[31m#{str}\e[0m"
end

$stdin.each_char do |c|
	if transfer.has_key?(c)
		print transfer[c]
	else
		print ansi_red(c)
	end
end
