#!/usr/bin/env ruby
#Written by Alex Gittmeier <gittemeier.alex@gmail.com>


def list_inverses(r)
	puts "under mod #{r}:"
	coprimes = (1..r).select{|x| x.gcd(r) == 1}
	coprimes.map do |a|
		inverse = coprimes.detect{|x| (a * x) % r == 1}
		puts "a = #{a}, a⁻¹ = #{inverse}"
	end
	puts
end

if $0 == __FILE__
	(2..).each do |i|
		list_inverses(i)
	end
end
